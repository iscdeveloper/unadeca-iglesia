<?php


use App\Models\Settings;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    public function run()
    {
        Settings::insert([
            'video_id' => 'afad',
            'countdown_time' => '18:00:00',
            'topic_title' => 'LA MÁS GLORIOSA ESPERANZA',
            'topic_desc' => 'Cómo enfrentar el futuro sin temor. El día cuando los problemas terminen y todos nuestros sueños se tornen realidad.',
            'options_title' => 'MI RESPUESTA DE AMOR A JESÚS',
            'option_1' => 'El tema de hoy me ha traído esperanza.',
            'option_2' => 'Quiero prepararme para la Segunda Venida de Cristo.',
            'option_3' => 'Quiero hacer planes de ser bautizado/a.',
        ]);
    }

}
