<?php

use App\Models\Country;
use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::insert([
            [
                "name" => "Estados Unidos"
            ],
            [
                "name" => "Mexico"
            ],
            [
                "name" => "Belize"
            ],
            [
                "name" => "El Salvador"
            ],
            [
                "name" => "Guatemala"
            ],
            [
                "name" => "Honduras"
            ],
            [
                "name" => "Nicaragua"
            ],
            [
                "name" => "Costa Rica"
            ],
            [
                "name" => "Panamá"
            ],
            [
                "name" => "Otro"
            ]
        ]);
    }
}
