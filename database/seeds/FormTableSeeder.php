<?php

use App\Models\Form;
use Illuminate\Database\Seeder;

class FormTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'adf',
            'lastname' => 'aretyyw',
            'phone' => '8554412',
            'email' => '',
            'country_id' => 2,
            'country_other' => '',
            'age' => '',
            'adventist' => 'Si',
            'study' => '',
            'petition' => 'adfasdfa as fasf asdf ds sasd fsaf',
            'date_answered' => '2020-05-20'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'yetrwwr',
            'lastname' => 'asdfacas',
            'phone' => '654321',
            'email' => 'm@email.com',
            'country_id' => 10,
            'country_other' => 'Colombia',
            'age' => '13 - 17',
            'adventist' => 'No',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-21'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'ureg',
            'lastname' => 'ms gbytefdgsd',
            'phone' => '7415962',
            'email' => '',
            'country_id' => 1,
            'country_other' => '',
            'age' => '',
            'adventist' => 'Si',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-15'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'oiaowej',
            'lastname' => 'qoiuq,c',
            'phone' => '5432',
            'email' => '',
            'country_id' => 3,
            'country_other' => '',
            'age' => '36+',
            'adventist' => 'Si',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-16'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'oqieua',
            'lastname' => 'oiqeadgq',
            'phone' => '34',
            'email' => '',
            'country_id' => 4,
            'country_other' => '',
            'age' => '',
            'adventist' => 'No',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-18'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'oiweru',
            'lastname' => ',mnzvznpo',
            'phone' => '42',
            'email' => '',
            'country_id' => 5,
            'country_other' => '',
            'age' => '',
            'adventist' => 'No',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-20'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'adfqewr',
            'lastname' => 'poiuyt',
            'phone' => '86327527',
            'email' => '',
            'country_id' => 6,
            'country_other' => '',
            'age' => '',
            'adventist' => 'Si',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-17'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'kjhgf',
            'lastname' => 'trew',
            'phone' => '234563821',
            'email' => '',
            'country_id' => 7,
            'country_other' => '',
            'age' => '',
            'adventist' => 'No',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-21'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'oiuy',
            'lastname' => 'dfghj',
            'phone' => '876543',
            'email' => '',
            'country_id' => 8,
            'country_other' => '',
            'age' => '',
            'adventist' => 'Si',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-19'
        ]);

        Form::create([
            'option_1' => false,
            'option_2' => false,
            'option_3' => true,
            'name' => 'asdf',
            'lastname' => 'jhtrwrgw',
            'phone' => '6543',
            'email' => '',
            'country_id' => 9,
            'country_other' => '',
            'age' => '',
            'adventist' => 'No',
            'study' => '',
            'petition' => '',
            'date_answered' => '2020-05-17'
        ]);

    }
}
