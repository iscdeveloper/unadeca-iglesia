<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BaseMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
        Schema::create('forms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('topic');
            $table->boolean('option_1')->nullable();
            $table->boolean('option_2')->nullable();
            $table->boolean('option_3')->nullable();
            $table->string('name');
            $table->string('lastname');
            $table->string('phone');
            $table->string('email')->nullable();
            $table->integer('country_id')->unsigned();
            $table->string('country_other')->nullable();
            $table->string('age')->nullable();
            $table->string('adventist')->nullable();
            $table->string('study')->nullable();
            $table->longText('petition')->nullable();
            $table->date('date_answered');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->timestamps();
        });
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('video_id');
            $table->time('countdown_time');
            $table->string('topic_title');
            $table->string('topic_desc');
            $table->string('options_title');
            $table->string('option_1');
            $table->string('option_2');
            $table->string('option_3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
