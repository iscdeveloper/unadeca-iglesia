<?php

namespace App\Http\Controllers;

use App\Exports\ReportExport;
use App\Models\Country;
use App\Models\Form;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Inertia\Response;
use Maatwebsite\Excel\Facades\Excel;
use function foo\func;

class FormController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function downloadExcel($id)
    {
        $item = Form::where('country_id', $id)->orderByDesc('created_at')
            ->get(['option_1', 'option_2', 'option_3', 'name', 'lastname', 'phone', 'email', 'country_id',
                'country_other', 'age', 'adventist', 'study', 'petition', 'date_answered']);

        $c = Country::where('id', $id)->pluck('name')->first();
        $fileName = 'reporte-' . $c . '.xlsx';

        return Excel::download(new ReportExport($item), $fileName);
    }

    public function downloadExcelAll()
    {
        $item = Form::get(['option_1', 'option_2', 'option_3', 'name', 'lastname', 'phone',
            'email', 'country_id', 'country_other', 'age', 'adventist', 'study', 'petition', 'date_answered']);

        return Excel::download(new ReportExport($item), 'reporte-total.xlsx');
    }

    public function getFiltered($id)
    {
        $filtered = Form::where('country_id', $id)->with('country')->orderByDesc('created_at')->get();

        $items[] = $filtered;
        return back()->with('success', $items);
    }

    public function getAll()
    {
        $all = Form::with('country')->get();

        $items[] = $all;
        return back()->with('success', $items);
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        $form = new Form();
        $form->topic = $request->topic;
        $form->option_1 = $request->option_1;
        $form->option_2 = $request->option_2;
        $form->option_3 = $request->option_3;
        $form->name = $request->name;
        $form->lastname = $request->lastname;
        $form->phone = $request->phone;
        $form->email = $request->email;
        $form->country_id = $request->country;
        if ($request->country === 10) {
            $form->country_other = $request->country_other;
        }
        $form->age = $request->age;
        $form->adventist = $request->adventist;
        $form->study = $request->study;
        $form->petition = $request->petition;
        $form->date_answered = Carbon::now()->format('Y-m-d');
        $form->save();
        DB::commit();

        return back();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
