<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Form;
use App\Models\Settings;
use Illuminate\Http\Request;
use Inertia\Inertia;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Inertia\Response
     */
    public function index()
    {
        $forms = Form::with('country')->orderByDesc('created_at')->get();
        $countries = Country::all();
        $settings = Settings::where('id', 1)->first();
        return Inertia::render('Home/Index', [
            'forms' => $forms,
            'countries' => $countries,
            'settings' => $settings
        ]);
    }
}
