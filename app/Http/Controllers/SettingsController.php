<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        $settings = new Settings();
        $settings->video_id = $request->video_id;
        $settings->countdown_time = $request->countdown_time;
        dd(json_encode($request->topic_title));
        $settings->topic_title = $request->topic_title;
        $settings->topic_desc = $request->topic_desc;
        $settings->options_title = $request->options_title;
        $settings->option_1 = $request->option_1;
        $settings->option_2 = $request->option_2;
        $settings->option_3 = $request->option_3;
        $settings->save();
        DB::commit();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        $settings = Settings::findOrFail($id);
        $settings->video_id = $request->video_id;
        $settings->countdown_time = $request->countdown_time;
        $settings->topic_title = $request->topic_title;
        $settings->topic_desc = $request->topic_desc;
        $settings->options_title = $request->options_title;
        $settings->option_1 = $request->option_1;
        $settings->option_2 = $request->option_2;
        $settings->option_3 = $request->option_3;
        $settings->save();
        DB::commit();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
