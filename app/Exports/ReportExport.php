<?php

namespace App\Exports;

use App\Models\Country;
use App\Models\Form;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ReportExport implements FromCollection, withHeadings
{
    Use Exportable;

    protected $items;

    public function __construct($items)
    {
        $this->items = $items;
    }

    public function headings(): array
    {
        return ["Opcion 1", "Opcion 2", "Opcion 3", "Nombres", "Apellidos", "Telefono", "Correo",
            "Pais", "Pais Otro", "Edad", "Adventista", "Estudia la Biblia", "Peticion", "Fecha"];
    }

//    public function array(): array
//    {
//        $data = [];
//
//            foreach ($this->items as $i) {
//                $option1 = ($i->option_1 == 0) ? 'No' : 'Si';
//                $option2 = ($i->option_2 == 0) ? 'No' : 'Si';
//                $option3 = ($i->option_3 == 0) ? 'No' : 'Si';
//
//                $country = Country::where('id', $i->country_id)->pluck('name')->first();
//
//                $data[] = [$option1, $option2, $option3, $i->name, $i->lastname, $i->phone, $i->email,
//                    $country, $i->country_other, $i->age, $i->adventist, $i->study, $i->petition,
//                    $i->date_answered];
//            }
//
//        return $data;
//    }

    /**
     * @return Collection
     */
    public function collection()
    {
        $data = [];

        foreach ($this->items as $i) {
            $option1 = ($i->option_1 == 0) ? 'No' : 'Si';
            $option2 = ($i->option_2 == 0) ? 'No' : 'Si';
            $option3 = ($i->option_3 == 0) ? 'No' : 'Si';

            $country = Country::where('id', $i->country_id)->pluck('name')->first();

            $data[] = [$option1, $option2, $option3, $i->name, $i->lastname, $i->phone, $i->email,
                $country, $i->country_other, $i->age, $i->adventist, $i->study, $i->petition,
                $i->date_answered];
        }

        return collect($data);
    }
}
