<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
