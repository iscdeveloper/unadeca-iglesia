<?php

use App\Exports\ReportExport;
use App\Models\Form;
use Maatwebsite\Excel\Facades\Excel;

Route::get('info', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login')->name('login.attempt')->uses('Auth\LoginController@login');

Route::post('/submit', 'FormController@store');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/logout')->name('logout')->uses('Auth\LoginController@logout');

    Route::post('/settings', 'SettingsController@store');
    Route::post('/settings/{id}', 'SettingsController@update');

    Route::get('/filter/{id}', 'FormController@getFiltered');
    Route::get('/getall', 'FormController@getAll');

    Route::get('/download/{id}', 'FormController@downloadExcel');
    Route::get('/download', 'FormController@downloadExcelAll');

});



