(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      counting: false,
      videoId: '',
      loadingLogin: false,
      loadingSurvey: false,
      dialogFormVisible: false,
      counter: false,
      index: 0,
      prevDisabled: true,
      nextDisabled: false,
      videos: [{
        video_id: 'YpxHYLAgFNY',
        topic_title: "LA MÁS GLORIOSA ESPERANZA",
        topic_desc: "Cómo enfrentar el futuro sin temor. El día cuando los problemas terminen y todos nuestros sueños se tornen realidad.",
        options_title: "MI RESPUESTA DE AMOR A JESÚS",
        option_1: "El tema de hoy me ha traído esperanza.",
        option_2: "Quiero prepararme para la Segunda Venida de Cristo.",
        option_3: "Quiero hacer planes de ser bautizado/a."
      }, {
        video_id: 'e6LNFgSwAJA',
        topic_title: "ESPERANZA FRENTE AL DOLOR Y EL SUFRIMIENTO",
        topic_desc: "Si Dios es bueno, ¿por qué sufren los inocentes? ¿Cómo puede permitirlo un Dios de amor? ¿Cuál es el origen del mal y el dolor? ¿Tendrá fin el mal? Si Dios es poderoso, ¿porqué no destruye a Satanás?",
        options_title: "MI RESPUESTA DE AMOR A JESÚS",
        option_1: "Gracias Jesús por venir a rescatarme.",
        option_2: "Acepto a Jesús como mi Salvador personal.",
        option_3: "Quiero entregar mi vida y voluntad a Jesús."
      }, {
        video_id: 'HL-e4D0oLks',
        topic_title: "ESPERANZA DESDE EL SANTUARIO CELESTIAL",
        topic_desc: "Las buenas nuevas acerca del Juicio de los siglos. ¿Qué está haciendo Jesús ahora antes de regresar?",
        options_title: "MI RESPUESTA DE AMOR A JESÚS",
        option_1: "Hoy acepto a Jesús como mi Salvador y Abogado.",
        option_2: "Quiero que mi nombre esté en el Libro de la Vida.",
        option_3: "Quiero hacer planes de ser bautizado/a."
      }, {
        video_id: 'otEfvqCrRzs',
        topic_title: "UN CERCO DE AMOR Y ESPERANZA",
        topic_desc: "La solución divina a la ola de maldad que azota al mundo",
        options_title: "MI RESPUESTA DE AMOR A JESÚS",
        option_1: "Acepto a Jesús como mi Savador personal.",
        option_2: "Por la gracia de Dios quiero guardar su Santa Ley y aceptar el sábado como el día del Señor.",
        option_3: "Quiero hacer planes de ser bautizado/a."
      }, {
        video_id: 'I0qxsc7G8Zw',
        topic_title: "ESPERANZA PARA UN NUEVO COMIENZO",
        topic_desc: "Tres pasos garantizados por Dios para vivir para siempre",
        options_title: "MI RESPUESTA DE AMOR A JESÚS",
        option_1: "Creo en Cristo y acepto su muerte en la cruz.",
        option_2: "Amo a Cristo y deseo guardar sus mandamientos.",
        option_3: "Quiero pertenecer a Cristo y ser bautizado."
      }, {
        video_id: 'eUo9ebLRNCU',
        topic_title: "UN SOLO DIOS, ¿POR QUÉ TANTAS RELIGIONES?",
        topic_desc: "¿De dónde salieron? ¿Son todas ellas caminos que conducen al mismo lugar? ¿Es el plan de Dios que haya tal variedad de caminos no importa cuáles sean?",
        options_title: "MI RESPUESTA DE AMOR A JESÚS",
        option_1: "Quiero aceptar toda la verdad bíblica",
        option_2: "Quiero que mi nombre esté en el Libro de la Vida.",
        option_3: "Quiero prepararme para un próximo bautismo."
      }, {
        video_id: 'A4j4sqfNqLA',
        topic_title: "ESPERANZA MÁS ALLÁ DE LA MUERTE",
        topic_desc: "¿Qué ocurre cuando una persona muere? ¿Hay vida después de la vida? ¿Podemos comunicarnos con nuestros seres queridos que han muerto? ¿Qué son las apariciones?",
        options_title: "MI RESPUESTA DE AMOR A JESÚS",
        option_1: "El tema de hoy me ha traído esperanza.",
        option_2: "Acepto a Jesús quien es la resurrección y la vida.",
        option_3: "Quiero pertenecer a Cristo y ser bautizado."
      }, {
        video_id: 'Hc9FxiGrCgg',
        topic_title: "EL PUEBLO DE LA ESPERANZA",
        topic_desc: "¿Tiene Dios una iglesia? ¿Podemos identificarla? ¿Son todas ellas caminos que conducen al mismo lugar? ¿Es el plan de Dios que haya tal variedad de caminos no importa cuáles sean?",
        options_title: "MI RESPUESTA DE AMOR A JESÚS",
        option_1: "Agradezco a Dios por darme la luz de su Palabra.",
        option_2: "Quiero ser parte del pueblo de Dios que tiene la fe de Jesús y guarda los mandamientos.",
        option_3: "Quiero pertenecer a Cristo y ser bautizado."
      }],
      loginForm: {
        username: '',
        password: ''
      },
      loginRules: {
        username: {
          required: true,
          message: 'Este campo es requerido',
          trigger: 'blur'
        },
        password: {
          required: true,
          message: 'Este campo es requerido',
          trigger: 'blur'
        }
      },
      surveyForm: {
        topic: 'LA MÁS GLORIOSA ESPERANZA',
        option_1: false,
        option_2: false,
        option_3: false,
        name: '',
        lastname: '',
        phone: '',
        email: '',
        country: '',
        country_other: '',
        age: '',
        adventist: '',
        study: '',
        petition: ''
      },
      surveyRules: {
        name: {
          required: true,
          message: 'Este campo es requerido',
          trigger: 'blur'
        },
        lastname: {
          required: true,
          message: 'Este campo es requerido',
          trigger: 'blur'
        },
        phone: [{
          required: true,
          message: 'Este campo es requerido',
          trigger: 'blur'
        }, {
          type: 'number',
          message: 'El numero de telefono debe ser solo numeros',
          trigger: 'blur'
        }],
        email: {
          type: 'email',
          message: 'Este campo debe estar formateado correctamente',
          trigger: 'blur'
        },
        country: {
          required: true,
          message: 'Este campo es requerido',
          trigger: 'blur'
        },
        country_other: {
          required: true,
          message: 'Este campo es requerido',
          trigger: 'blur'
        },
        adventist: {
          required: true,
          message: 'Este campo es requerido',
          trigger: 'blur'
        }
      },
      countries: [],
      settings: {},
      hours: [],
      contacts: [{
        name: 'UNIÓN DE GUATEMALA',
        number: '+502 2226 3500'
      }, {
        name: 'UNIÓN DE BELIZE',
        number: '+501 223-5938'
      }, {
        name: 'UNIÓN DE EL SALVADOR',
        number: '+503 2559 2222'
      }, {
        name: 'UNIÓN DE HONDURAS',
        number: '+504 2232-0696'
      }, {
        name: ' UNIÓN DE NICARAGUA Y COSTA RICA',
        number: '+506 2442 2042'
      }, {
        name: 'UNIÓN DE PANAMÁ',
        number: '+507 314-1510'
      }]
    };
  },
  computed: {
    player: function player() {
      return this.$refs.youtube.player;
    },
    computedTime: function computedTime() {
      var now = new Date();
      var todaytime = new Date(now.getFullYear(), now.getMonth(), now.getDate(), this.hours[0], this.hours[1], this.hours[2]);
      var timeLeft = todaytime - now;

      if (timeLeft <= 0) {
        this.reloadCountdown();
        var tomorrowtime = new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1, this.hours[0], this.hours[1], this.hours[2]);
        timeLeft = tomorrowtime - now;
      }

      return timeLeft;
    }
  },
  methods: {
    prev: function prev() {
      if (this.index !== 0) {
        this.index -= 1;
        this.surveyForm.topic = this.videos[this.index].topic_title;

        if (this.index !== 7) {
          this.nextDisabled = false;
        }
      } else {
        this.prevDisabled = true;
      }
    },
    next: function next() {
      if (this.index !== 7) {
        this.index += 1;
        this.surveyForm.topic = this.videos[this.index].topic_title;

        if (this.index !== 0) {
          this.prevDisabled = false;
        }
      } else {
        this.nextDisabled = true;
      }
    },
    download: function download() {
      window.open("https://unadecanet-my.sharepoint.com/:f:/g/personal/wcolindres_unadeca_net/EveqlA1N0l5Nt0jSw6HcakcBRWiGpZDT2zSCWQU3JOYs8w?e=kodM5S");
    },
    reloadCountdown: function reloadCountdown() {
      this.reloadCounter += 1;
    },
    startCountdown: function startCountdown() {
      this.counting = true;
    },
    handleCountdownEnd: function handleCountdownEnd() {
      this.reloadCountdown();
      this.counting = true;
    },
    openDialog: function openDialog() {
      this.dialogFormVisible = true;
    },
    cancel: function cancel() {
      this.loginForm = {
        username: '',
        password: ''
      };
      this.dialogFormVisible = false;
    },
    submitLogin: function submitLogin() {
      var _this = this;

      this.$refs.loginForm.validate(function (valid) {
        if (valid) {
          _this.loadingLogin = true;

          _this.$inertia.post('/login', {
            username: _this.loginForm.username,
            password: _this.loginForm.password
          }).then(function () {
            return _this.loadingLogin = false;
          });
        } else {
          return false;
        }
      });
    },
    reset: function reset() {
      this.surveyForm.option_1 = false;
      this.surveyForm.option_2 = false;
      this.surveyForm.option_3 = false;
      this.surveyForm.petition = '';
    },
    submitSurvey: function submitSurvey() {
      var _this2 = this;

      this.$refs.surveyForm.validate(function (valid) {
        if (valid) {
          _this2.loadingSurvey = false;

          _this2.$inertia.post('/submit', _this2.surveyForm).then(function () {
            _this2.$message({
              message: 'Gracias por responder!',
              type: 'success'
            }), _this2.$refs.surveyForm.resetFields(), // this.reset(),
            _this2.loadingSurvey = false;
          }, function (res) {
            _this2.$message({
              message: 'Ha sucedido un error, intente de nuevo',
              type: 'error'
            });
          });
        } else {
          return false;
        }
      });
    }
  },
  mounted: function mounted() {
    if (this.$page.countries) {
      this.countries = this.$page.countries;
    }

    if (this.$page.settings) {
      this.settings = this.$page.settings;
    }

    if (this.$page.hours) {
      this.hours = this.$page.hours;
    }

    document.title = "Mi Futuro";
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "el-container",
    [
      _c(
        "el-header",
        [
          _c(
            "el-row",
            { staticClass: "vertical-center" },
            [
              _c(
                "el-col",
                { attrs: { xl: 22, lg: 22, md: 22, sm: 22, xs: 22 } },
                [
                  _c("div", { staticClass: "header-title" }, [
                    _vm._v("Iglesia Adventista del Séptimo Día")
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "el-col",
                { attrs: { xl: 2, lg: 2, md: 2, sm: 2, xs: 2 } },
                [
                  _c(
                    "el-button",
                    {
                      staticClass: "fa-pull-right loginbtn",
                      attrs: { type: "text" },
                      on: { click: _vm.openDialog }
                    },
                    [_vm._v("\n                    Entrar\n                ")]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "container" },
        [
          _c(
            "div",
            { staticClass: "youtube-container" },
            [
              _c(
                "el-card",
                {
                  staticStyle: {
                    width: "100%",
                    border: "none",
                    "background-color": "#1E3260",
                    color: "#FFF",
                    "border-radius": "5px 5px 0 0",
                    "font-family": "Times New Roman, Times, serif",
                    "font-size": "20px",
                    "text-align": "center"
                  },
                  attrs: { shadow: "never" }
                },
                [
                  _c("div", [
                    _vm._v("\n                    UN "),
                    _c(
                      "span",
                      {
                        staticStyle: {
                          "font-family": "Edwardian Script ITC",
                          "font-size": "40px",
                          "margin-right": "7px"
                        }
                      },
                      [_vm._v("F")]
                    ),
                    _vm._v(
                      "UTURO\n                    CON\n                    "
                    ),
                    _c(
                      "span",
                      {
                        staticStyle: {
                          "font-family": "Edwardian Script ITC",
                          "font-size": "40px",
                          "margin-right": "5px"
                        }
                      },
                      [_vm._v("E")]
                    ),
                    _vm._v("SPERANZA\n                ")
                  ]),
                  _vm._v(" "),
                  _c("div", [
                    _c("span", [
                      _vm._v("CON EL ORADOR "),
                      _c("span", { staticStyle: { "font-size": "22px" } }, [
                        _vm._v("Pr. ROBERT COSTA")
                      ])
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c("youtube", {
                ref: "youtube",
                staticStyle: { padding: "0", margin: "0" },
                attrs: {
                  resize: true,
                  "fit-parent": true,
                  "video-id": _vm.videos[_vm.index].video_id
                }
              }),
              _vm._v(" "),
              _c(
                "el-card",
                {
                  staticStyle: {
                    width: "100%",
                    border: "none",
                    "background-color": "#ffca06",
                    color: "#FFF",
                    "text-align": "center",
                    "margin-bottom": "0.25em"
                  },
                  attrs: { shadow: "never" }
                },
                [
                  _c(
                    "el-link",
                    {
                      attrs: { underline: false },
                      on: { click: _vm.download }
                    },
                    [_vm._v("Descarga Estudios Bíblicos.")]
                  )
                ],
                1
              ),
              _vm._v(
                "\n\n            " +
                  _vm._s(_vm.surveyForm.topic) +
                  "\n\n            "
              ),
              _c(
                "el-row",
                [
                  _c(
                    "el-col",
                    { attrs: { xl: 12, lg: 12, md: 12, sm: 24, xs: 24 } },
                    [
                      _c(
                        "el-button",
                        {
                          attrs: {
                            type: "primary",
                            disabled: _vm.prevDisabled
                          },
                          on: { click: _vm.prev }
                        },
                        [
                          _c("i", { staticClass: "fas fa-arrow-left" }),
                          _vm._v(" "),
                          _c("span", [_vm._v("Anterior")])
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-col",
                    {
                      staticStyle: { "text-align": "right" },
                      attrs: { xl: 12, lg: 12, md: 12, sm: 24, xs: 24 }
                    },
                    [
                      _c(
                        "el-button",
                        {
                          attrs: {
                            type: "primary",
                            disabled: _vm.nextDisabled
                          },
                          on: { click: _vm.next }
                        },
                        [
                          _c("span", [_vm._v("Siguiente")]),
                          _vm._v(" "),
                          _c("i", { staticClass: "fas fa-arrow-right" })
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.counter
                ? _c(
                    "div",
                    { staticStyle: { "text-align": "center" } },
                    [
                      _c("el-divider"),
                      _vm._v(" "),
                      _c("h1", [_vm._v("Próximo Culto")]),
                      _vm._v(" "),
                      _c("countdown", {
                        key: _vm.reloadCounter,
                        attrs: { time: _vm.computedTime },
                        scopedSlots: _vm._u(
                          [
                            {
                              key: "default",
                              fn: function(props) {
                                return [
                                  _c(
                                    "ul",
                                    {
                                      staticStyle: {
                                        "padding-inline-start": "0px"
                                      }
                                    },
                                    [
                                      _c(
                                        "li",
                                        { staticClass: "countdown-name" },
                                        [
                                          _c(
                                            "span",
                                            { staticClass: "countdown-num" },
                                            [_vm._v(_vm._s(props.hours))]
                                          ),
                                          _vm._v(
                                            "Horas\n                            "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "li",
                                        { staticClass: "countdown-name" },
                                        [
                                          _c(
                                            "span",
                                            { staticClass: "countdown-num" },
                                            [_vm._v(_vm._s(props.minutes))]
                                          ),
                                          _vm._v(
                                            "Minutos\n                            "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "li",
                                        { staticClass: "countdown-name" },
                                        [
                                          _c(
                                            "span",
                                            { staticClass: "countdown-num" },
                                            [_vm._v(_vm._s(props.seconds))]
                                          ),
                                          _vm._v(
                                            "Segundos\n                            "
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                ]
                              }
                            }
                          ],
                          null,
                          false,
                          2550056981
                        )
                      })
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "main_coverage" }, [
            _c("div", { staticClass: "sidebar" }, [
              _c("div", { staticClass: "elements_container" }, [
                _c(
                  "span",
                  { staticStyle: { width: "30%", "text-align": "center" } },
                  [
                    _c("img", {
                      staticStyle: { width: "80%" },
                      attrs: {
                        src: __webpack_require__(/*! ../../../../public/img/EscritoEsta_Logo.png */ "./public/img/EscritoEsta_Logo.png")
                      }
                    })
                  ]
                ),
                _vm._v(" "),
                _c(
                  "span",
                  { staticStyle: { width: "30%", "text-align": "center" } },
                  [
                    _c("img", {
                      staticStyle: { width: "35%" },
                      attrs: {
                        src: __webpack_require__(/*! ../../../../public/img/IASD_Circulo_Logo.png */ "./public/img/IASD_Circulo_Logo.png")
                      }
                    })
                  ]
                ),
                _vm._v(" "),
                _c(
                  "span",
                  { staticStyle: { width: "30%", "text-align": "center" } },
                  [
                    _c("img", {
                      staticStyle: { width: "70%" },
                      attrs: {
                        src: __webpack_require__(/*! ../../../../public/img/UNADECA_Logos_Blanco.png */ "./public/img/UNADECA_Logos_Blanco.png")
                      }
                    })
                  ]
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "div",
              { staticClass: "field" },
              [
                _c(
                  "el-form",
                  {
                    ref: "surveyForm",
                    attrs: { model: _vm.surveyForm, rules: _vm.surveyRules }
                  },
                  [
                    _c("h2", [
                      _vm._v(_vm._s(_vm.videos[_vm.index].topic_title))
                    ]),
                    _vm._v(" "),
                    _c("p", [_vm._v(_vm._s(_vm.videos[_vm.index].topic_desc))]),
                    _vm._v(" "),
                    _c("el-divider"),
                    _vm._v(" "),
                    _c("h2", [
                      _vm._v(_vm._s(_vm.videos[_vm.index].options_title))
                    ]),
                    _vm._v(" "),
                    _c(
                      "el-form-item",
                      {
                        staticStyle: { "margin-bottom": "0" },
                        attrs: { prop: "option_1" }
                      },
                      [
                        _c("el-checkbox", {
                          model: {
                            value: _vm.surveyForm.option_1,
                            callback: function($$v) {
                              _vm.$set(_vm.surveyForm, "option_1", $$v)
                            },
                            expression: "surveyForm.option_1"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticStyle: {
                              "padding-left": "0.5em",
                              width: "90%",
                              "overflow-wrap": "break-word"
                            }
                          },
                          [
                            _vm._v(
                              "\n                            " +
                                _vm._s(_vm.videos[_vm.index].option_1) +
                                "\n                        "
                            )
                          ]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-form-item",
                      {
                        staticStyle: { "margin-bottom": "0" },
                        attrs: { prop: "study_ck" }
                      },
                      [
                        _c("el-checkbox", {
                          attrs: { prop: "option_2" },
                          model: {
                            value: _vm.surveyForm.option_2,
                            callback: function($$v) {
                              _vm.$set(_vm.surveyForm, "option_2", $$v)
                            },
                            expression: "surveyForm.option_2"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticStyle: {
                              "padding-left": "0.5em",
                              width: "90%",
                              "overflow-wrap": "break-word"
                            }
                          },
                          [
                            _vm._v(
                              "\n                            " +
                                _vm._s(_vm.videos[_vm.index].option_2) +
                                "\n                        "
                            )
                          ]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-form-item",
                      {
                        staticStyle: { "margin-bottom": "0" },
                        attrs: { prop: "choice_ck" }
                      },
                      [
                        _c("el-checkbox", {
                          attrs: { prop: "option_3" },
                          model: {
                            value: _vm.surveyForm.option_3,
                            callback: function($$v) {
                              _vm.$set(_vm.surveyForm, "option_3", $$v)
                            },
                            expression: "surveyForm.option_3"
                          }
                        }),
                        _vm._v(" "),
                        _c(
                          "span",
                          {
                            staticStyle: {
                              "padding-left": "0.5em",
                              width: "90%",
                              "overflow-wrap": "break-word"
                            }
                          },
                          [
                            _vm._v(
                              "\n                            " +
                                _vm._s(_vm.videos[_vm.index].option_3) +
                                "\n                        "
                            )
                          ]
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("el-divider"),
                    _vm._v(" "),
                    _c("h2", [_vm._v("Información de Contacto")]),
                    _vm._v(" "),
                    _c(
                      "el-row",
                      { attrs: { gutter: 20 } },
                      [
                        _c(
                          "el-col",
                          { attrs: { xl: 12, lg: 12, md: 12, sm: 24, xs: 24 } },
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: {
                                  "label-position": "right",
                                  label: "Nombres",
                                  prop: "name"
                                }
                              },
                              [
                                _c("el-input", {
                                  attrs: { placeholder: "Nombres" },
                                  model: {
                                    value: _vm.surveyForm.name,
                                    callback: function($$v) {
                                      _vm.$set(_vm.surveyForm, "name", $$v)
                                    },
                                    expression: "surveyForm.name"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "el-col",
                          { attrs: { xl: 12, lg: 12, md: 12, sm: 24, xs: 24 } },
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: {
                                  "label-position": "right",
                                  label: "Apellidos",
                                  prop: "lastname"
                                }
                              },
                              [
                                _c("el-input", {
                                  attrs: { placeholder: "Apellidos" },
                                  model: {
                                    value: _vm.surveyForm.lastname,
                                    callback: function($$v) {
                                      _vm.$set(_vm.surveyForm, "lastname", $$v)
                                    },
                                    expression: "surveyForm.lastname"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-row",
                      { attrs: { gutter: 20 } },
                      [
                        _c(
                          "el-col",
                          { attrs: { xl: 12, lg: 12, md: 12, sm: 12, xs: 24 } },
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: {
                                  "label-position": "right",
                                  xl: 12,
                                  lg: 12,
                                  md: 12,
                                  sm: 12,
                                  xs: 24,
                                  label: "Celular",
                                  prop: "phone"
                                }
                              },
                              [
                                _c("el-input", {
                                  attrs: {
                                    placeholder: "Celular",
                                    type: "phone"
                                  },
                                  model: {
                                    value: _vm.surveyForm.phone,
                                    callback: function($$v) {
                                      _vm.$set(
                                        _vm.surveyForm,
                                        "phone",
                                        _vm._n($$v)
                                      )
                                    },
                                    expression: "surveyForm.phone"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "el-col",
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: {
                                  "label-position": "right",
                                  label: "Correo Electrónico",
                                  prop: "email"
                                }
                              },
                              [
                                _c("el-input", {
                                  attrs: { placeholder: "Correo Electrónico" },
                                  model: {
                                    value: _vm.surveyForm.email,
                                    callback: function($$v) {
                                      _vm.$set(_vm.surveyForm, "email", $$v)
                                    },
                                    expression: "surveyForm.email"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-row",
                      { attrs: { gutter: 20 } },
                      [
                        _c(
                          "el-col",
                          { attrs: { xl: 12, lg: 12, md: 12, sm: 12, xs: 24 } },
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: {
                                  "label-position": "right",
                                  prop: "country",
                                  label: "¿De donde eres?"
                                }
                              },
                              [
                                _c(
                                  "el-select",
                                  {
                                    staticStyle: { width: "100%" },
                                    attrs: { placeholder: "País" },
                                    model: {
                                      value: _vm.surveyForm.country,
                                      callback: function($$v) {
                                        _vm.$set(_vm.surveyForm, "country", $$v)
                                      },
                                      expression: "surveyForm.country"
                                    }
                                  },
                                  _vm._l(_vm.countries, function(item) {
                                    return _c("el-option", {
                                      key: item.id,
                                      attrs: {
                                        label: item.name,
                                        value: item.id
                                      }
                                    })
                                  }),
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _vm.surveyForm.country === 10
                          ? _c(
                              "el-col",
                              {
                                attrs: {
                                  xl: 12,
                                  lg: 12,
                                  md: 12,
                                  sm: 12,
                                  xs: 24
                                }
                              },
                              [
                                _c(
                                  "el-form-item",
                                  {
                                    attrs: {
                                      "label-position": "right",
                                      prop: "country_other",
                                      label: "Escriba su país"
                                    }
                                  },
                                  [
                                    _c("el-input", {
                                      attrs: { placeholder: "País" },
                                      model: {
                                        value: _vm.surveyForm.country_other,
                                        callback: function($$v) {
                                          _vm.$set(
                                            _vm.surveyForm,
                                            "country_other",
                                            $$v
                                          )
                                        },
                                        expression: "surveyForm.country_other"
                                      }
                                    })
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-row",
                      [
                        _c(
                          "el-col",
                          { attrs: { xl: 24, lg: 24, md: 24, sm: 24, xs: 24 } },
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: { prop: "age", label: "Rango de Edad:" }
                              },
                              [
                                _c(
                                  "el-radio-group",
                                  {
                                    model: {
                                      value: _vm.surveyForm.age,
                                      callback: function($$v) {
                                        _vm.$set(_vm.surveyForm, "age", $$v)
                                      },
                                      expression: "surveyForm.age"
                                    }
                                  },
                                  [
                                    _c("el-radio", {
                                      attrs: { label: "13 - 17" }
                                    }),
                                    _vm._v(" "),
                                    _c("el-radio", {
                                      attrs: { label: "18 - 35" }
                                    }),
                                    _vm._v(" "),
                                    _c("el-radio", { attrs: { label: "36+" } })
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-row",
                      [
                        _c(
                          "el-col",
                          { attrs: { xl: 24, lg: 24, md: 24, sm: 24, xs: 24 } },
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: {
                                  prop: "adventist",
                                  label: "¿Eres Adventista?"
                                }
                              },
                              [
                                _c(
                                  "el-radio-group",
                                  {
                                    model: {
                                      value: _vm.surveyForm.adventist,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.surveyForm,
                                          "adventist",
                                          $$v
                                        )
                                      },
                                      expression: "surveyForm.adventist"
                                    }
                                  },
                                  [
                                    _c("el-radio", { attrs: { label: "Si" } }),
                                    _vm._v(" "),
                                    _c("el-radio", { attrs: { label: "No" } })
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-row",
                      [
                        _c(
                          "el-col",
                          { attrs: { xl: 24, lg: 24, md: 24, sm: 24, xs: 24 } },
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: {
                                  prop: "study",
                                  label: "¿Quieres estudiar la Biblia?"
                                }
                              },
                              [
                                _c(
                                  "el-radio-group",
                                  {
                                    model: {
                                      value: _vm.surveyForm.study,
                                      callback: function($$v) {
                                        _vm.$set(_vm.surveyForm, "study", $$v)
                                      },
                                      expression: "surveyForm.study"
                                    }
                                  },
                                  [
                                    _c("el-radio", { attrs: { label: "Si" } }),
                                    _vm._v(" "),
                                    _c("el-radio", { attrs: { label: "No" } })
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "el-row",
                      [
                        _c(
                          "el-col",
                          { attrs: { xl: 24, lg: 24, md: 24, sm: 24, xs: 24 } },
                          [
                            _c(
                              "el-form-item",
                              {
                                attrs: {
                                  prop: "petition",
                                  label: "¿Tienes un pedido de oracion?"
                                }
                              },
                              [
                                _c("el-input", {
                                  attrs: { type: "textarea", rows: 2 },
                                  model: {
                                    value: _vm.surveyForm.petition,
                                    callback: function($$v) {
                                      _vm.$set(_vm.surveyForm, "petition", $$v)
                                    },
                                    expression: "surveyForm.petition"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    staticClass: "fa-pull-right",
                    attrs: { slot: "footer" },
                    slot: "footer"
                  },
                  [
                    _c(
                      "el-button",
                      {
                        attrs: { type: "warning", loading: _vm.loadingSurvey },
                        on: {
                          click: function($event) {
                            return _vm.submitSurvey()
                          }
                        }
                      },
                      [
                        _c("span", { staticClass: "icon-question" }, [
                          _vm._v("Enviar")
                        ]),
                        _vm._v(" "),
                        _c("i", { staticClass: "fas fa-paper-plane" })
                      ]
                    )
                  ],
                  1
                )
              ],
              1
            )
          ]),
          _vm._v(" "),
          _c(
            "el-dialog",
            { attrs: { visible: _vm.dialogFormVisible, "show-close": false } },
            [
              _c(
                "el-form",
                {
                  ref: "loginForm",
                  attrs: {
                    "label-position": "top",
                    model: _vm.loginForm,
                    rules: _vm.loginRules
                  }
                },
                [
                  _c(
                    "el-form-item",
                    { attrs: { prop: "username" } },
                    [
                      _c("el-input", {
                        attrs: {
                          type: "text",
                          placeholder: "Usuario",
                          autocomplete: "off",
                          clearable: ""
                        },
                        model: {
                          value: _vm.loginForm.username,
                          callback: function($$v) {
                            _vm.$set(_vm.loginForm, "username", $$v)
                          },
                          expression: "loginForm.username"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "el-form-item",
                    { attrs: { prop: "password" } },
                    [
                      _c("el-input", {
                        attrs: {
                          type: "password",
                          "show-password": "",
                          placeholder: "Contraseña",
                          autocomplete: "off",
                          clearable: ""
                        },
                        model: {
                          value: _vm.loginForm.password,
                          callback: function($$v) {
                            _vm.$set(_vm.loginForm, "password", $$v)
                          },
                          expression: "loginForm.password"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.$page.prop.errors
                ? _c(
                    "el-row",
                    { staticClass: "mb-2" },
                    _vm._l(_vm.$page.prop.errors, function(error) {
                      return _c(
                        "div",
                        [
                          _c("el-alert", {
                            attrs: {
                              closable: false,
                              title: error[0],
                              type: "error",
                              "show-icon": ""
                            }
                          })
                        ],
                        1
                      )
                    }),
                    0
                  )
                : _vm._e(),
              _vm._v(" "),
              _c(
                "span",
                { attrs: { slot: "footer" }, slot: "footer" },
                [
                  _c(
                    "el-button",
                    {
                      staticClass: "login-button",
                      attrs: { type: "info" },
                      on: {
                        click: function($event) {
                          return _vm.cancel()
                        }
                      }
                    },
                    [
                      _c("span", { staticClass: "icon-question" }, [
                        _vm._v("Cancelar")
                      ]),
                      _vm._v(" "),
                      _c("i", { staticClass: "fas fa-times-circle" })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "el-button",
                    {
                      staticClass: "login-button",
                      attrs: { type: "warning", loading: _vm.loadingLogin },
                      on: {
                        click: function($event) {
                          return _vm.submitLogin()
                        }
                      }
                    },
                    [
                      _c("span", { staticClass: "icon-question" }, [
                        _vm._v("Entrar")
                      ]),
                      _vm._v(" "),
                      _c("i", { staticClass: "fas fa-check-circle" })
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("el-card", { staticClass: "card-info", attrs: { shadow: "never" } }, [
        _c("div", [
          _c("h2", [
            _vm._v(
              "Llama a este número en tu país para encontrar una iglesia más cercana."
            )
          ])
        ]),
        _vm._v(" "),
        _c(
          "div",
          [
            _c(
              "el-table",
              { staticStyle: { width: "100%" }, attrs: { data: _vm.contacts } },
              [
                _c("el-table-column", { attrs: { prop: "name" } }),
                _vm._v(" "),
                _c("el-table-column", { attrs: { prop: "number" } })
              ],
              1
            )
          ],
          1
        )
      ]),
      _vm._v(" "),
      _c(
        "footer",
        {
          staticStyle: {
            "background-color": "#ffca06",
            padding: "0.5em",
            "text-align": "center"
          }
        },
        [_c("h3", [_vm._v("Diseño y soporte: ISC DEVELOPERS SERVICES (2020)")])]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js":
/*!********************************************************************!*\
  !*** ./node_modules/vue-loader/lib/runtime/componentNormalizer.js ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return normalizeComponent; });
/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file (except for modules).
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

function normalizeComponent (
  scriptExports,
  render,
  staticRenderFns,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier, /* server only */
  shadowMode /* vue-cli only */
) {
  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (render) {
    options.render = render
    options.staticRenderFns = staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = 'data-v-' + scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = shadowMode
      ? function () { injectStyles.call(this, this.$root.$options.shadowRoot) }
      : injectStyles
  }

  if (hook) {
    if (options.functional) {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      var originalRender = options.render
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return originalRender(h, context)
      }
    } else {
      // inject component registration as beforeCreate hook
      var existing = options.beforeCreate
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    }
  }

  return {
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ "./public/img/EscritoEsta_Logo.png":
/*!*****************************************!*\
  !*** ./public/img/EscritoEsta_Logo.png ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/EscritoEsta_Logo.png?844c303db4582ab315473bde95cfd29e";

/***/ }),

/***/ "./public/img/IASD_Circulo_Logo.png":
/*!******************************************!*\
  !*** ./public/img/IASD_Circulo_Logo.png ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/IASD_Circulo_Logo.png?d129b3cf40fcfaa0d496eae02c98a45b";

/***/ }),

/***/ "./public/img/UNADECA_Logos_Blanco.png":
/*!*********************************************!*\
  !*** ./public/img/UNADECA_Logos_Blanco.png ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/UNADECA_Logos_Blanco.png?9877ca85621bdd3800a4352e2edad6b7";

/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue":
/*!*******************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=a2ac2cea& */ "./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/Pages/Auth/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&":
/*!**************************************************************************!*\
  !*** ./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=a2ac2cea& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/Pages/Auth/Login.vue?vue&type=template&id=a2ac2cea&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_a2ac2cea___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);